-- Exercise 1

toDigits 	 :: Integer -> [Integer]
toDigitsRev  :: Integer -> [Integer]
getLastDigit :: Integer -> Integer

toDigits x = reverse (toDigitsRev x)

toDigitsRev x
    | x <= 0 = []
    | otherwise = getLastDigit (x) : toDigitsRev ((x - getLastDigit(x)) `div` 10)

getLastDigit x
	| x == 0 = 0
    | length (show x) == 1 = x
    | otherwise = getLastDigit( x - (10^ (length (show x) -1) * (x `div` 10^ (length (show x) -1))))

--Exercise 2

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOtherRev :: [Integer] -> [Integer]

doubleEveryOther xs = reverse (doubleEveryOtherRev (reverse xs))

doubleEveryOtherRev [] = []
doubleEveryOtherRev (x:[]) = [x]
doubleEveryOtherRev (x:y:xs)= x: (y*2) : doubleEveryOtherRev (xs)

-- Exercise 3

sumDigits :: [Integer] -> Integer
sumDigits' :: [Integer] -> Integer
getSingleDigitsList :: [Integer] -> [Integer]

sumDigits [] = 0
sumDigits (x:[]) = x
sumDigits xs = sumDigits' (getSingleDigitsList xs)

sumDigits' (x:xs)
   | (x:xs) == [] = 0
   | (x:xs) == (x:[]) = x
   | otherwise = x + sumDigits'(xs) 

getSingleDigitsList [] = []
getSingleDigitsList (x:xs) = (toDigits x) ++ (getSingleDigitsList xs)

-- Exercise 4

validate :: Integer -> Bool

validate x = sumDigits(doubleEveryOther(toDigits x)) `mod` 10 == 0